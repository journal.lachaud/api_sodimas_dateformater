# Cloner le projet

`git clone https://gitlab.com/journal.lachaud/api_sodimas_dateformater.git`

# Installer les dépendances

`composer install`

# Lancer l'API

`symfony serve`
