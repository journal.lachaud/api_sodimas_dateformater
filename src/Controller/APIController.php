<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;


class APIController extends AbstractController
{
    /**
     * @Route("/arab-to-roman", name="return_roman_number")
     */
    public function dateInRoman()
    {
        //Get number
        $date = $_GET['date'];

        //Sépare les chiffre de la date
        $nums = explode("/", $date);

        $res = "";
        $prefix = "";
       
        //parcours chaque chiffre de la date
        foreach ($nums as $index) {
            $res .= $prefix;
            $res .= $this->returnRomanNumber($index);

            $prefix = " - ";

    }

        return new JsonResponse(array('romannumber' => $res));


    }


   protected function returnRomanNumber($num)
    {
        // Vérification de l'entier
        $n = intval($num);
        $res = '';

        // Declaration du tableau
        $tabDecEnRom = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
            'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
            'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);

        // Boucle de convertion
        foreach ($tabDecEnRom as $romain => $valeur)
        {
            $trouver = intval($n / $valeur);
            $res .= str_repeat($romain, $trouver);
            $n = $n % $valeur;
        }

        return $res;
    }



}
